package test;

import static java.lang.Math.PI;

public class Kolo extends Figura implements IFigura {
    private double r;
    private double obw;

    public Kolo(String name, Float r) {
        super(name);
        this.r = r;
        this.obw = 2 * PI * r;
        this.pole = PI * r * r;
    }

    @Override
    public double getPole() {
        return pole;
    }

    @Override
    public double getObwod() {
        return obw;
    }

    public double getR() {
        return r;
    }
}
