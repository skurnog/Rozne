package test;

public interface IFigura {
    public double getPole();
    public double getObwod();
}
