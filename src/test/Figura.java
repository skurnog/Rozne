package test;

public class Figura {
    protected double pole;
    protected String name;

    public Figura(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Figura: " + name;
    }
}
