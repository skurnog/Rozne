package test;

public class Kwadrat extends Figura implements IFigura{
    private double a;
    public static int costam = 7;

    public Kwadrat(double a){
        super("kwadrat");
        this.a = a;
    }

    @Override
    public double getPole(){
        return a*a;
    }

    @Override
    public double getObwod(){
        return 4*a;
    }

    public double getPrzekatna(){
        return Math.sqrt(a);
    }
}