package zadanko;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Main {

    public static List<Integer> lista = Arrays.asList(1, 2, 4, 3, 3);

    public static void main (String[] args) {
        check(lista);
    }

    private static boolean check(List<Integer> tablica){

        Integer element = tablica.get(0);
        checkCycle(element);

        System.out.println(element);
        return true;
    }

    private static boolean checkCycle(Integer indexElement){
        Integer startValue = lista.get(indexElement);
        Integer currentValue = lista.get(startValue);

        while(true) {
            if (startValue == currentValue) return true;
            currentValue = lista.get(currentValue);
            System.out.println(currentValue);
        }
    }

}
